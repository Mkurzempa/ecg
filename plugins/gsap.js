import gsap from "gsap";
import Vue from "vue";
Vue.use(gsap);

if (process.client) {
  gsap.registerPlugin();
}
